1.
Trigger the CI/CD system to run test first (good)
Encourage discussion for shared understanding (good)
Trigger code reviews (good)

2. It isn't (good)
To get resources (bad)

3. Not much (good)
A test infraestructure (bad)

4. Before (good)

5. Break up the project into smaller pieces (good)
Bring in a speaker (bad)

6. The need for increased integration and system testing (good)

7. Blue-Green Pattern (good)

8. It shows where and why specifics tests failed
Automatically formulates integration tests for new code (bad)

9. Rollback (good)

10. Create a diagram of your current pipeline with your interdisciplinary team 
Create a plan with your interdisciplinary team (bad)