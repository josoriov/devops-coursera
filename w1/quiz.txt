1. Increases the speed (good)

2. Testing (good)
Planning (bad)
Decreasing velocity (bad)

3. Improve focus (good) 
Show how speed increases (bad)
Show how cost decreases (bad)

4. Test ideas (good)
Decrease turbulence (bad)
Create better relationship (bad)

5. 
	- Improve interface (good)
	- Reduce repetitive (good)
	- Learn how to build (good)

6.
	- Reduce manual work (good)
	- Reduce overhead (good)
	- Improve interface (good)
	- Improve utilization (bad)

7. Test the same thing (good)
Increase Velocity (bad)

8. Agile user stories (good)

9. Artifact repository (bad)
Version control (good)

10. Implement processes (good)

